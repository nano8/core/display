<?php

namespace laylatichy\nano\core;

use ReflectionClass;
use Smarty;

final class Display {
    private static Display $_instance;

    private static Config  $config;

    private function __construct(
        private Smarty $smarty = new Smarty(),
    ) {
        self::$config = Config::getInstance();

        $this->setCacheSettings();
        $this->setSmartyDir();
    }

    public static function init(): Display {
        if (!isset(self::$_instance)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public static function assign(array $data): Display {
        self::$_instance->smarty->assign($data);

        return self::$_instance;
    }

    public static function clear(): void {
        array_map(
            callback: 'unlink',
            array: array_filter((array)glob(pattern: self::$config::getRoot() . '/../.smarty/compiled/*'))
        );
        array_map(
            callback: 'unlink',
            array: array_filter((array)glob(pattern: self::$config::getRoot() . '/../.smarty/cache/*'))
        );
    }

    public function fetch(string $file = ''): ?string {
        if (!$file) {
            $file = $this->getCaller(debug_backtrace());
        }

        $tpl = $this->smarty->fetch(template: "{$file}.tpl");

        $this->smarty->clearAllAssign();

        return $tpl;
    }

    public function display(string $file = ''): ?string {
        if (!$file) {
            $file = $this->getCaller(debug_backtrace());
        }

        $tpl = $this->smarty->display(template: "{$file}.tpl");

        $this->smarty->clearAllAssign();

        return $tpl;
    }

    private function setCacheSettings(): void {
        $this->smarty->caching = Smarty::CACHING_OFF;
    }

    private function setSmartyDir(): void {
        $this->smarty->setCompileDir(compile_dir: self::$config::getRoot() . '/../.smarty/compiled/');
        $this->smarty->setCacheDir(cache_dir: self::$config::getRoot() . '/../.smarty/cache/');
        $this->smarty->setTemplateDir(template_dir: self::$config::getRoot() . '/../views/');
    }

    private function getCaller(array $backtrace): string {
        $caller = end($backtrace);

        $class  = $caller['class'];
        $method = $caller['function'];

        $reflectionClass  = new ReflectionClass(objectOrClass: $class);
        $reflectionMethod = $reflectionClass->getMethod(name: $method);

        $classAttributes  = $reflectionClass->getAttributes(name: __CLASS__);
        $methodAttributes = $reflectionMethod->getAttributes(name: __CLASS__);

        if (!empty($classAttributes)) {
            $attribute = $classAttributes[0];

            $dirAttribute = $attribute->getArguments()[0];
        }

        if (!empty($methodAttributes)) {
            $attribute = $methodAttributes[0];

            $fileAttribute = $attribute->getArguments()[0];
        }

        $dir  = $dirAttribute ?? str_replace(search: '\\', replace: '/', subject: $class);
        $file = $fileAttribute ?? $method;

        return $dir . '/' . $file;
    }
}
