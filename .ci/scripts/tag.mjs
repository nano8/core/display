#!/usr/bin/env zx

import 'zx/globals';

$.verbose = false;

(async () => {
    const name = require('../../composer.json').name;
    console.log(chalk.blue.bold(`started tagging module: ${chalk.green.bold(name)}`));

    const describe = await $`git describe --tags \`git rev-list --tags --max-count=1\``;

    console.log(chalk.blue.bold(`latest version: ${chalk.green.bold(describe.stdout)}`));

    const version = describe
        .stdout
        .replace('v', '')
        .replace(/(\r\n|\n|\r)/gm, '')
        .split('.')
        .map(e => parseInt(e));

    version[2]++;

    const newVersion = version
        .join('.');

    console.log(chalk.blue.bold(`updating tag to: ${chalk.green.bold(newVersion)}`));

    const commit = await $`git rev-parse HEAD`;

    try {
        await $`git describe --contains ${commit.stdout}`;

        console.log(chalk.blue.bold(`cannot describe - this means commit is untagged`));

        await $`git tag -a ${newVersion} -m "release - ${newVersion}"`;

        console.log(chalk.blue.bold(`pushing tag...`));
        await $`git push --tags`;
    } catch {
        console.log(chalk.red.bold(`Already a tag on this commit`));
    }

    console.log(chalk.blue.bold(`tagging done: ${chalk.green.bold(newVersion)}`));
})();
