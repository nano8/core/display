### nano/core/display

[![release](https://img.shields.io/packagist/v/laylatichy/nano-core-display?style=flat-square)](https://packagist.org/packages/laylatichy/nano-core-display)
[![PHPStan](https://img.shields.io/badge/PHPStan-Level%20max-brightgreen.svg?style=flat&logo=php)](https://shields.io/#/)
[![Total Downloads](https://img.shields.io/packagist/dt/laylatichy/nano-core-display?style=flat-square)](https://packagist.org/packages/laylatichy/nano-core-display)

##### CI STATUS

-   dev [![pipeline status](https://gitlab.com/nano8/core/display/badges/dev/pipeline.svg)](https://gitlab.com/nano8/core/display/-/commits/dev)
-   master [![pipeline status](https://gitlab.com/nano8/core/display/badges/master/pipeline.svg)](https://gitlab.com/nano8/core/display/-/commits/master)
-   release [![pipeline status](https://gitlab.com/nano8/core/display/badges/release/pipeline.svg)](https://gitlab.com/nano8/core/display/-/commits/release)

#### Install

-   `composer require laylatichy/nano-core-display`
